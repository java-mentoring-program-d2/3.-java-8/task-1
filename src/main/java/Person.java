import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Person {
    private static final Comparator<Person> comparatorByName = Comparator.comparing(Person::getName);
    private static final Comparator<Person> comparatorByAge = Comparator.comparing(Person::getAge);

    private final String name;
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Sergey", 36));
        people.add(new Person("Ivan", 31));
        people.add(new Person("Igor", 29));
        people.add(new Person("Timur", 26));
        people.add(new Person("Anton", 41));
        people.add(new Person("Mike", 37));

        people.stream()
                .sorted(comparatorByName.thenComparing(comparatorByAge))
                .forEach(System.out::println);
    }
}
